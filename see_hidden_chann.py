#!/usr/bin/env python3
import json
import requests

# TO FILL MANUALLY
SERVER_ID = ""  # 68735847036XXXXXXX
AUTHORIZATION_TOKEN = ""  # XXXXXXXXXXXXXXXXXXXXXXXX.XXXXXX.XXXXXXXXXXXXXXXXXXXXXXXXXXX


def main():
    url = f"https://discordapp.com/api/v6/guilds/{SERVER_ID}/channels"

    headers = {
        "Authorization": AUTHORIZATION_TOKEN,
    }

    channels = requests.get(url, headers=headers).json()

    channel_type = {
        0: "Text channel",
        1: "Direct message",
        2: "Voice channel",
        3: "Group DM",
        4: "Server Category",
        5: "News channel",
        6: "Store channel",
    }

    for channel in channels:
        print(f"Name: {channel['name']}")
        print(f"URL: https://discordapp.com/channels/{SERVER_ID}/{channel['id']}")
        print(f"Type: {channel_type[channel['type']]}")

        if "topic" in channel.keys() and channel["topic"]:
            print(f"Topic: {channel['topic']}")

        print(f"Position: {channel['position']}")

        if channel["nsfw"]:
            print(f"NSFW: ON")
        print("=" * 30)


if __name__ == "__main__":
    main()
