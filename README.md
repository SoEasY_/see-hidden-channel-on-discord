# See Hidden Channel on Discord 

**See hidden channel on discord by using the API.**

### Usage:

```bash
$ git clone https://gitlab.com/xanhacks/see-hidden-channel-on-discord

$ cd see-hidden-channel-on-discord

$ python3 see_hidden_chann.py
```

**Before run it.**

Fill the SERVER\_ID and AUTHORIZATION\_TOKEN in the python script.

**SERVER\_ID**

Turn on "Developer Mode" in Discord -> Settings -> Appearance -> Advanced

Right click on the server icon & click on "Copy ID"

**AUTHORIZATION\_TOKEN**

Go to [discordapp.com](https://discordapp.com) and check your requests header (console developper).

Find a header named "Authorization" and there is your key.

### References:

[Discord API](https://discordapp.com/developers/docs/intro)

[Discord API (channel)](https://discordapp.com/developers/docs/resources/channel)
